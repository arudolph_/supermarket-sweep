#!/usr/bin/env python3.6

import json
import maps

FILE_NAME = 'stores.json'

def get_list():
    with open(FILE_NAME) as f:
        return json.load(f)

def add_store(node_id, address, points):
    current = get_list()
    current.append({'id': node_id, 'address': address, 'points': points})
    with open(FILE_NAME, 'w') as f:
        json.dump(current, f)

if __name__ == '__main__':
    import sys
    points = int(sys.argv[1])
    while True:
        node_id = input('id?:\n')
        if not node_id:
            break
        address = input('address?:\n')
        add_store(node_id, address, points)
