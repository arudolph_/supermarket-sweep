#!/usr/bin/env python3.6

import json
import os
import sys

class Locations:
    FILE_NAME = 'locations.json'

    def __init__(self):
        try:
            with open(self.FILE_NAME) as f:
                self.data = json.load(f)
        except FileNotFoundError:
            sys.stderr.write('Run locations.py and enter start/end/stores\n')
            exit(-1)
        self.start = self.data['start']
        self.end = self.data['end']
        self.stores = self.data['stores']

if __name__ == '__main__':
    data = {}
    data['start'] = input('start?:\n')
    data['end'] = input('end?:\n')
    data['stores'] = stores = []
    for points in [10, 20, 30, 40]:
        print(f'enter {points}-point stores, followed by enter')
        while True:
            store = {'points': points}
            store['name'] = input('name?:\n')
            if not store['name']:
                break
            store['address'] = input('address?:\n')
            stores.append(store)
    with open(Locations.FILE_NAME, 'w') as f:
        json.dump(data, f)
