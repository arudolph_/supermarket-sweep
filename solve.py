#!/usr/bin/env python3.6

import argparse
import locations
import googlemaps
import sys
import json
import random

class Solver:
    def __init__(self, multiplier, shopping_time, debug=False):
        self.duration = 4 * 3600

        self.multiplier = multiplier
        self.shopping_time = shopping_time
        self.debug = debug

        self.locations = locations.Locations()
        random.shuffle(self.locations.stores)
        self.end_node = {'name': 'End', 'address': self.locations.end}

        self._initialize_distance_map()
        self.recursive_invocations = 0

    def solve(self):
        start_node = {'name': 'Start', 'address': self.locations.start}
        initial_state = {
            'visited': [start_node],
            'remaining': self.duration,
            'remaining_at_finish': 0,
            'points': 0,
            'depth': 0,
        }
        self.best_global_path = initial_state
        return self._solve(initial_state)

    def _solve(self, state):
        self.recursive_invocations += 1
        if self.debug and (self.recursive_invocations % 100000 == 0):
            sys.stderr.write(f'{self.recursive_invocations} _solve calls\n')

        best_path = state
        remaining = state['remaining']
        visited = state['visited']
        end = self.end_node
        for node in self.locations.stores:
            if node in visited:
                continue
            travel_time = self._biking_time(visited[-1], node)
            visit_time = travel_time + self.shopping_time
            post_visit_remaining = remaining - visit_time
            finish_time = self._biking_time(node, end)
            if finish_time > post_visit_remaining:
                continue
            new_state = {
                'visited': state['visited'] + [node],
                'remaining': post_visit_remaining,
                'remaining_at_finish': post_visit_remaining - finish_time,
                'points': state['points'] + node['points'],
                'depth': state['depth'] + 1,
            }

            sub_path = self._solve(new_state)

            if cmp_key(sub_path) > cmp_key(best_path):
                best_path = sub_path

                if cmp_key(best_path) > cmp_key(self.best_global_path):
                    print_result(best_path)
                    self.best_global_path = best_path

        return best_path

    def _biking_time(self, nodeA, nodeB):
        ptA, ptB = nodeA['address'], nodeB['address']
        base_time = self.distance_map[self._key(ptA, ptB)]
        return int(self.multiplier * base_time)

    def _key(self, a, b):
        return f'{a}|{b}'

    def _initialize_distance_map(self):
        try:
            # memoize on disk
            with open('distance.json') as f:
                self.distance_map = json.load(f)
                return
        except FileNotFoundError:
            pass

        try:
            with open('api_key.txt') as f:
                gmaps = googlemaps.Client(key=f.read().strip())
        except FileNotFoundError:
            sys.stderr.write('create api_key.txt with google server key\n'
                ' (get one at https://console.developers.google.com/)\n')
            exit(-1)

        self.distance_map = {}
        addresses = [s['address'] for s in self.locations.stores]
        dests = addresses + [self.locations.end]
        # REVIEW: 'distance_matrix' tends to crap out...do the loop ourselves
        for origin in addresses + [self.locations.start]:
            sys.stderr.write(f'processing {origin}\n')
            for ix, dest in enumerate(dests):
                sys.stderr.write(f'\tloading distance for {dest}\n')
                v = gmaps.directions(origin, dest, mode='bicycling')
                travel_time = v[0]['legs'][0]['duration']['value']
                self.distance_map[self._key(origin, dest)] = travel_time

        with open('distance.json', 'w') as f:
            json.dump(self.distance_map, f)


def cmp_key(path):
    return (path['points'], path['remaining_at_finish'])


def print_result(result):
    print()
    print(f'Points: {result["points"]}')
    print(f'Remaining time (seconds): {result["remaining"]}')
    print(f'Remaining at finish: {result["remaining_at_finish"]}')
    for v in result['visited']:
        p = v.get('points')
        name = f'{v["name"]} ({v["address"]})'
        if p:
            name += f' {p}'
        print(name)

def solve():
    parser = argparse.ArgumentParser(description='Supermarket Street Sweeper')
    parser.add_argument('--multiplier', type=float, default=.8,
            help='.8 => 80%% of google biking estimates')
    parser.add_argument('--shopping-time', type=int, default=300,
            help="average time in the store")
    parser.add_argument('--debug', action='store_true',
            help="whether to print 'alive' messages to stderr")
    args = parser.parse_args()
    s = Solver(
        multiplier=args.multiplier,
        shopping_time=args.shopping_time,
        debug=args.debug
    )
    result = s.solve()
    print_result(result)


if __name__ == '__main__':
    solve()
