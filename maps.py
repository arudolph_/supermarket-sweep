#!/usr/bin/env python3.6

import json
import googlemaps

with open('credentials.json') as f:
    client = googlemaps.Client(key=json.load(f)['api_key'])
